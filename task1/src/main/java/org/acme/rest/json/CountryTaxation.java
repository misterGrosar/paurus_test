package org.acme.rest.json;

public class CountryTaxation {
    private TaxationType taxationType;
    private Double taxationRate;
    private Double taxationAmount;

    public CountryTaxation(TaxationType taxationType, Double taxationRate, Double taxationAmount) {
        this.taxationType = taxationType;
        this.taxationRate = taxationRate;
        this.taxationAmount = taxationAmount;
    }

    public TaxationType getTaxationType() {
        return taxationType;
    }

    public Double getTaxationRate() {
        return taxationRate;
    }

    public Double getTaxationAmount() {
        return taxationAmount;
    }
}
