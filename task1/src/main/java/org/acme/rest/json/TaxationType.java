package org.acme.rest.json;

public enum TaxationType {
    RATE,
    AMOUNT
}
