package org.acme.rest.json;

public class Incoming {

    public Integer traderId;
    public Double playedAmount;
    public Double odd;

    public Incoming() {
    }

    public Incoming(Integer traderId, Double playedAmount, Double odd) {
        this.traderId = traderId;
        this.playedAmount = playedAmount;
        this.odd = odd;
    }
}
