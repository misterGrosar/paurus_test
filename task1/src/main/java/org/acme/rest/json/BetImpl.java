package org.acme.rest.json;

import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class BetImpl {

    public Outgoing bet(Incoming incoming) throws IllegalArgumentException {
        if (incoming.playedAmount <= 0) {
            throw new IllegalArgumentException("Payment amount cannot be les or equal then 0.0");
        }

        Outgoing outgoingRes = new Outgoing();

        CountryTaxation countryTaxation =  getCountryTaxation(incoming.traderId);
        Double winningAmount = incoming.playedAmount * incoming.odd;

        outgoingRes.possibleReturnAmountBefTax = winningAmount;

        if (TaxationType.RATE.equals(countryTaxation.getTaxationType())) {
            outgoingRes.possibleReturnAmountAfterTax = winningAmount - (winningAmount * countryTaxation.getTaxationRate());
        } else if (TaxationType.AMOUNT.equals(countryTaxation.getTaxationType())) {
            outgoingRes.possibleReturnAmountAfterTax = winningAmount - countryTaxation.getTaxationAmount();
        } else {
            throw new IllegalArgumentException("Wrong taxation type");
        }

        outgoingRes.taxRate = countryTaxation.getTaxationRate();
        outgoingRes.taxAmount = countryTaxation.getTaxationAmount();

        return outgoingRes;
    }

    private CountryTaxation getCountryTaxation(Integer trackerId) throws IllegalArgumentException {
        // Tukaj bi v realni aplikaciji pridobil podatke iz baze (predpostavljam, da je uporabnik vezan na državo oziroma
        // in ta ima shranjeni vrednosti taxRate in taxAmount)

        // Za katero državo in kakšen tip obdavčitve gre, bi lahko vezali tudi na query parameter pri klicu servisa,
        // v katerega bi podali identifikator države, ali pa bi razširili vhodni JSON in v njem podali identifikator države
        // namesto v query parameter
        if (trackerId == 0) {
            return new CountryTaxation(TaxationType.RATE, 0.1, null);
        } else if (trackerId == 1) {
            return new CountryTaxation(TaxationType.AMOUNT, null, 2.0);
        } else {
            throw new IllegalArgumentException("Wrong trackerID");
        }
    }
}
