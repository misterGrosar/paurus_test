package org.acme.rest.json;

public class Outgoing {
    public Double possibleReturnAmount;
    public Double possibleReturnAmountBefTax;
    public Double possibleReturnAmountAfterTax;
    public Double taxRate;
    public Double taxAmount;

    public Outgoing() {

    }

    public Outgoing(Double possibleReturnAmount, Double possibleReturnAmountBefTax,
                    Double possibleReturnAmountAfterTax, Double taxRate, Double taxAmount) {
        this.possibleReturnAmount = possibleReturnAmount;
        this.possibleReturnAmountBefTax = possibleReturnAmountBefTax;
        this.possibleReturnAmountAfterTax = possibleReturnAmountAfterTax;
        this.taxRate = taxRate;
        this.taxAmount = taxAmount;
    }
}
