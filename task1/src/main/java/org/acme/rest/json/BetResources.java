package org.acme.rest.json;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("/bet")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BetResources {

    @Inject
    BetImpl betImpl;

    public BetResources() {
    }

    @POST
    public Outgoing postBet(Incoming incoming) {
        return betImpl.bet(incoming);
    }
}
