<h4>Kako pognati servise</h4>
1. Poženeš ukaz <code>mvn clean install</code>. Program potrebuje Maven verzije 3.8.1 in Javo 8.
2. Po uspešnem buildu poženeš ukaz: <code>mvn compile quarkus:dev</code> 
<br/>Aplikacija se zažene na http://localhost:8080

<h4>Testni primeri</h4>
Testni primeri so v mapi postman. Uvozi se jih v Postman in požene.
