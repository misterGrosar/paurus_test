import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class MatchTracker {

    private static final Map<String, List<Match>> currentlyProcessingMatches = Collections.synchronizedMap(new HashMap<>(2500));
//    private static final Map<String, List<Match>> currentlyProcessingMatches = new ConcurrentHashMap<>();

    private static Connection con = null;
    private static Statement stmt = null;
    private static final String INSERT_STATEMENT = "INSERT INTO `matches` (`ID`, `MATCH_ID`, `MARKET_ID`, `OUTCOME_ID`, " +
            "`SPECIFIERS`, `DATE_INSERT`) VALUES (NULL, ?, ?, ?, ?, ?)";

    public static void main(String args[]) {
        long startTime = System.currentTimeMillis();
        BufferedReader fileBuffReader = null;

        try {
            // Worker thread pool
            ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() - 1);
            String currentLineStr;

            // Naloži jdbc gonilnik
            Class.forName("com.mysql.jdbc.Driver");

            // task2 is the database name, root is the username widhout password
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/task2?rewriteBatchedStatements=true", "root", "");
            stmt = con.createStatement();

            fileBuffReader = new BufferedReader(new FileReader("fo_random.txt"));

            boolean firstRow = true;

            while ((currentLineStr = fileBuffReader.readLine()) != null) {
                // Preskoči prvo vrstico v datoteki, če vsebuje nazive stolpcev
                if (firstRow && currentLineStr.contains("MATCH_ID")) {
                    firstRow = false;
                    continue;
                }

                String[] data = currentLineStr.split("\\|");

                if (data.length == 0) {
                    continue;
                }

                Match match = new Match(data[0].replace("'", ""), Integer.parseInt(data[1]),
                        data[2].replace("'", ""),
                        String.join("|", Arrays.asList(data).subList(3, data.length)).replace("'", ""));

                // Dodaj evete tekem v seznam trenutno procesiranih eventov.
                // Za vsako prebrano tekmo preverim ali se kateri od njenih eventov še procesira. V primeru,
                // da se procesiranje eventov za to tekmo še izvaja, dodam naslednji njen prebrani event v čakalno vrsto.
                // Ta event se bo procesiral takoj po koncu procesiranja predhodnega eventa iste tekme.
                currentlyProcessingMatches.compute(match.getMatchId(), (k, value) -> {
                    if (value == null) {
                        value = new LinkedList<>(Arrays.asList(match));
                        // Poženem izvajanje procesiranja za to tekmo na svoji procesorski niti
                        service.execute(new MatchWorker(match));
                    } else {
                        value.add(match);
                    }

                    return value;
                });
            }

            service.shutdown();

            // Blocks until all tasks have completed execution after a shutdown request,
            // or the timeout occurs, or the current thread is interrupted, whichever happens first.
            service.awaitTermination(60L, TimeUnit.SECONDS);

        } catch (IOException e) {
            System.out.println("Napaka pri branju datoteke: " + e);
        } catch (InterruptedException e) {
            System.out.println("Napaka pri ustavljanju threadov: " + e);
        } catch (ClassNotFoundException e) {
            System.out.println("Napaka pri nalaganju jdbc gonilnika: " + e);
        } catch (SQLException e) {
            System.out.println("SQL napaka: " + e);
        } finally {
            try {
                if (fileBuffReader != null) {
                    fileBuffReader.close();
                }
            } catch (IOException e) {
                System.out.println("Napaka pri zapiranju datoteke: " + e);
            }
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Napaka pri zapiranju bazne povezave: " + e);;
            }
        }

        System.out.format("Processed in %d milliseconds\n", System.currentTimeMillis() - startTime);
    }

    public static class MatchWorker implements Runnable {
        private final Match match;

        public MatchWorker(Match match) {
            this.match = match;
        }

        @Override
        public void run() {
            try {
                PreparedStatement ps  = con.prepareStatement(INSERT_STATEMENT);

                while (currentlyProcessingMatches.get(match.getMatchId()).size() > 0) {
                    Match currMatch = ((LinkedList<Match>)currentlyProcessingMatches.get(match.getMatchId())).getFirst();

                    try {
                        if (ps != null) {
                            ps.setString(1, currMatch.getMatchId());
                            ps.setInt(2, currMatch.getMarketId());
                            ps.setString(3, currMatch.getOutcomeId());
                            ps.setString(4, currMatch.getSpecifiers());
                            ps.setLong(5, System.currentTimeMillis());
                            ps.addBatch();
                        }
                    } catch (SQLException e) {
                        System.out.println("Napaka pri zapisu tekme z MATCH_ID = " + currMatch.getMatchId() +
                                " v bazo. Napaka: " + e);
                    }

                    // Odstranim že procesiran event iz čakalne vrste
                    currentlyProcessingMatches.compute(match.getMatchId(), (k, v) -> {
                        if (v != null && v.size() > 0) {
                            ((LinkedList<Match>) v).removeFirst();
                        }
                        return v;
                    });
                }

                try {
                    if (ps != null) {
                        ps.executeBatch();
                    }
                } catch (SQLException e) {
                    System.out.println("Napaka pri batch zapisu tekme z MATCH_ID: " + match.getMatchId() + ". Napaka: " + e);
                }

                // Na tej točki ni več eventov za to tekmo, zato odstranim ključ za tekmo iz čakalne vrste
                currentlyProcessingMatches.remove(match.getMatchId());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
